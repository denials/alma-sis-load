#!/usr/bin/env python

import csv
import logging


def get_barcodes(mapf):
    """
    Returns a dictionary mapping LU IDs to barcodes

    * mapf: path to a CSV file containing ID to barcode mappings

    \o luid_to_barcodes.csv
    COPY (
      SELECT au.ident_value, ac.barcode
      FROM actor.usr au
        INNER JOIN actor.card ac ON au.card = ac.id
      WHERE au.deleted IS FALSE
        AND au.ident_value ~ '^0'
      ORDER BY 1
    ) TO STDOUT (FORMAT CSV, DELIMITER ',');
    \o
    """

    barcodes = {}
    with open(mapf) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        for row in csv_reader:
            barcodes[row[0]] = row[1]

    return barcodes


def map_status(univ_id, status):
    if status == "A":
        status = "ACTIVE"
    elif status == "I":
        status = "INACTIVE"
    else:
        logging.warning(
            "{}: status is '{}' but should be A or I".format(univ_id, status)
        )

    return status


def map_user_group(univ_id, user_group):
    if user_group == "UG":
        user_group = "Undergraduate"
    elif user_group == "GR":
        user_group = "Graduate"
    elif user_group == "FACULTY":
        user_group = "Faculty"
    elif user_group == "STAFF":
        user_group = "Staff"
    else:
        logging.error(
            "{}: user_group is '{}' but should be UG, G, FACULTY, or STAFF".format(
                univ_id, user_group
            )
        )

    return user_group


def map_lang(univ_id, lang):
    if lang == "E":
        lang = "en"
    elif lang == "F":
        lang = "fr"
    else:
        logging.warning(
            "{}: lang is '{}' but should be E or F; defaulting to English".format(
                univ_id, lang
            )
        )
        lang = "en"
    return lang


def map_barcode(univ_id, barcodes):
    "Returns the barcode mapped from an LU ID"

    if univ_id in barcodes:
        return barcodes[univ_id]

    logging.error("No barcode found for LU ID {}".format(univ_id))


def generate_sis_xml(inf, outf, barcodes):
    """
    Generates v.2 SIS XML for Alma import

    * inf: input file of student data
    * outf: output file of SIS XML
    * barcodes: dict of LU ID to barcodes
    """

    with open(inf, newline="", encoding="latin") as csvf, open(
        outf, "w", encoding="utf-8"
    ) as sisfile:
        rdr = csv.reader(csvf, delimiter="|")

        sisfile.write(
            """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<users>"""
        )

        line = 0
        for (
            univ_id,
            primary_id,
            first_name,
            last_name,
            user_group,
            lang,
            status,
            email,
        ) in rdr:
            # active is always ACTIVE so we hardcode it
            # pref_name is thrown away

            line = line + 1
            logging.debug(line)

            # if univ_id.value not in ("0003692", "0099062", "0105133"):
            # testing import mode
            #    continue
            univ_id = univ_id.strip()
            barcode = map_barcode(univ_id, barcodes)
            user_group = map_user_group(univ_id, user_group.strip().upper())
            lang = map_lang(univ_id, lang.strip().upper())
            status = map_status(univ_id, status.strip().upper())
            if not primary_id:
                print("No primary_id for {}".format(univ_id))
                continue
            primary_id = primary_id.lower().strip()
            first_name = first_name.strip()
            last_name = last_name.strip()
            if not email:
                print("No email for {}".format(univ_id))
                continue
            email = email.lower().strip()
            expiry_date = get_expiry_date(user_group)

            sisfile.write(
                generate_sis_record(
                    barcode,
                    univ_id,
                    primary_id,
                    first_name,
                    last_name,
                    user_group,
                    lang,
                    email,
                    status,
                    expiry_date,
                )
            )
        sisfile.write("\n</users>")


def get_expiry_date(user_group):
    """
    Calculate expiry date based on account type

    Give a little wiggle room outside of the 120 day loan period because we're not
    sure how often we'll be able to do loads.
    """

    import datetime

    # offset: number of days before accounts expire
    offset = 150
    if user_group in ("Faculty", "Staff"):
        offset = 300
    return "{}Z".format(datetime.date.today() + datetime.timedelta(days=offset))


def generate_sis_record(
    barcode,
    univ_id,
    primary_id,
    first_name,
    last_name,
    user_group,
    lang,
    email,
    status,
    expiry_date,
):
    """
    Generates a SIS record the dumbest way possible

    Really should escape &<> but is anyone really going to Little Bobby Tables*
    this data? Yes this will probably bite me in the ass.

    * https://xkcd.com/327/
    """

    # Minimal record for import
    user_record = """
    <user>
        <record_type>PUBLIC</record_type>
        <primary_id>{primary_id}</primary_id>
        <first_name>{first_name}</first_name>
        <middle_name></middle_name>
        <last_name>{last_name}</last_name>
        <user_group>{user_group}</user_group>
        <preferred_language>{lang}</preferred_language>
        <expiry_date>{expiry_date}</expiry_date>
        <account_type>EXTERNAL</account_type>
        <external_id>SIS_temp</external_id>
        <status>{status}</status>
        <contact_info>
            <addresses/>
            <emails>
                <email preferred="true" segment_type="External">
                    <email_address>{email}</email_address>
                    <email_types>
                        <email_type>personal</email_type>
                        <email_type>work</email_type>
                        <email_type>school</email_type>
                        <email_type>alternative</email_type>
                    </email_types>
                </email>
            </emails>
            <phones/>
        </contact_info>
        <user_identifiers>
            <user_identifier segment_type="External">
                <id_type>BARCODE</id_type>
                <value>{barcode}</value>
                <status>{status}</status>
            </user_identifier>
            <user_identifier segment_type="External">
                <id_type>OTHER_ID_1</id_type>
                <value>{univ_id}</value>
                <status>{status}</status>
            </user_identifier>
        </user_identifiers>
    </user>"""

    return user_record.format(
        barcode=barcode,
        univ_id=univ_id,
        primary_id=primary_id,
        first_name=first_name,
        last_name=last_name,
        user_group=user_group,
        lang=lang,
        email=email,
        expiry_date=expiry_date,
        status=status,
    )


def main():
    import datetime

    logging.basicConfig(
        filename="processing.log", encoding="utf-8", level=logging.WARNING
    )

    # offset: number of days before accounts expire
    # give a little wiggle room outside of the 120 day loan period because we're not
    # sure how often we'll be able to do loads
    offset = 150
    expiry_date = "{}Z".format(datetime.date.today() + datetime.timedelta(days=offset))
    barcodes = get_barcodes("luid_to_barcodes.csv")
    # test file
    # generate_sis_xml("testload.txt", "testload.xml", barcodes, expiry_date)
    # students
    generate_sis_xml("usudbury_faculty.csv", "sisload.xml", barcodes)
    # staff and faculty
    # generate_sis_xml("alma.fs.txt", "sis_fac_load.xml", barcodes)


if __name__ == "__main__":
    main()
